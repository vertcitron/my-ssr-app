import { createApplication } from './main'
import { SSRApplication } from './models'
import { appStore } from './store/app'

export default (context: any): Promise<SSRApplication> => {
  return new Promise((resolve, reject) => {
    const { app, router } = createApplication()

    router.push(context.url)
    router.isReady()
      .then(() => {
        const matchedComponents = router.currentRoute.value.matched
        if (!matchedComponents.length) {
          return reject(new Error('404'))
        }
        const state = {
          app: appStore.getState()
        }
        return resolve({ app, router, state })
      }).catch(() => reject)
  })
}
