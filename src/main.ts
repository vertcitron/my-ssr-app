import { createApp, createSSRApp } from 'vue'
import { Router } from 'vue-router'
import { createApplicationRouter } from '@/router'
import { isSSR } from '@/utils'
import Application from '@/App.vue'
import PageMeta from './components/helpers/PageMeta.vue'
import PageMetaTeleport from './components/helpers/PageMetaTeleport.vue'
import { SSRApplication } from './models'

export function createApplication (): SSRApplication {
  const app = isSSR() ? createSSRApp(Application) : createApp(Application)

  const router: Router = createApplicationRouter()

  app.use(router)

  app.component('PageMeta', PageMeta)
  app.component('PageMetaTeleport', PageMetaTeleport)

  return { app, router }
}
