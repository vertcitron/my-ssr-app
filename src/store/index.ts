import { reactive, readonly } from 'vue'
import { isSSR } from '@/utils'

export abstract class Store<T extends Record<string, unknown>> {
  protected state: T

  constructor () {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const data = isSSR() ? this.data() : this.hydrate((window as any).__STATE__ || {})
    this.state = reactive(data) as T
  }

  protected abstract hydrate (state: Record<string, unknown>): T

  protected abstract data(): T

  public getState (): T {
    return readonly(this.state) as T
  }
}
