import { Store } from '@/store'
import { AppState } from '@/models'

class AppStore extends Store<AppState> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected hydrate (state: Record<string, unknown>): any {
    return state.app || this.data()
  }

  protected data (): AppState {
    return { fetching: false }
  }

  set fetching (fetching: boolean) {
    this.state.fetching = fetching
  }
}

export const appStore = new AppStore()
