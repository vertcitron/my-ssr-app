import { App } from 'vue'
import { Router } from 'vue-router'

export interface AppState extends Record<string, unknown> {
  fetching: boolean
}

export interface SSRApplication {
  app: App<Element>
  router: Router
  state?: { app: AppState; }
}
