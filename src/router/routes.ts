import { Component } from 'vue'

export const routes = [
  {
    path: '/',
    name: 'home',
    component: (): Promise<Component> => import(/* webpackChunkName: "Home" */ '../pages/Home.vue')
  }
]
