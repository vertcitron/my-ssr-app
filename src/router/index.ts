import { createMemoryHistory, createWebHistory, createRouter, Router } from 'vue-router'
import { isSSR } from '@/utils'
import { routes } from './routes'

export function createApplicationRouter (): Router {
  const router = createRouter({
    history: isSSR() ? createMemoryHistory() : createWebHistory(),
    routes
  })
  return router
}
